import Container from "typedi";
import { SuccessResponse } from "../../../utils";
import { MovieService } from "../services/movie.service";

exports.listAllMovies = async (req, reply) => {
  try {
    const movieService = Container.get(MovieService);

    const movies = await movieService.findAll();

    return reply
      .code(200)
      .send(SuccessResponse("Fetched movies successfully", { movies } ));
  } catch (error) {
    reply.send(error);
  }
};

exports.findMovie = async (req, reply) => {
  try {
    const movieService = Container.get(MovieService);

    const movie = await movieService.findById(req.params.movie_id);

    return reply
      .code(200)
      .send(SuccessResponse("Fetched movie successfully", movie));
  } catch (error) {
    reply.send(error);
  }
};