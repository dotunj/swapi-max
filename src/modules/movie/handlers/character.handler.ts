import Container from "typedi";
import { SuccessResponse } from "../../../utils";
import { CharacterService } from "../services/character.service";

exports.listAllCharacters = async (req, reply) => {
  try {
    const characterService = Container.get(CharacterService);

    const { characters, metaData } = await characterService.findAll(
      req.params.movie_id,
      {
        sort: req?.query?.sort,
        filter: req?.query?.filter,
      }
    );

    return reply
      .code(200)
      .send(
        SuccessResponse("Fetched characters successfully", {
          characters,
          meta_data: metaData,
        })
      );
  } catch (error) {
    reply.send(error);
  }
};
