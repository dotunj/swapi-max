import Container from "typedi";
import { SuccessResponse } from "../../../utils";
import { CommentService } from "../services/comment.service";

exports.CreateComment = async (req, reply) => {
  try {
    const commentService = Container.get(CommentService);

    const comment = await commentService.create({
      movie_id: req.params?.movie_id,
      message: req.body?.message,
      ip_address: req?.ip,
    });

    return reply
      .code(201)
      .send(SuccessResponse("Comment created successfully", comment));
  } catch (error) {
    reply.send(error);
  }
};

exports.ListComments = async (req, reply) => {
  try {
    const commentService = Container.get(CommentService);

    const comments = await commentService.findAll(req.params?.movie_id);

    return reply
      .code(200)
      .send(SuccessResponse("Retrieved comments successfully", { comments }));
  } catch (error) {
    reply.send(error);
  }
};

exports.FindComment = async (req, reply) => {
  try {
    const commentService = Container.get(CommentService);

    const comment = await commentService.findById(
      req.params?.movie_id,
      req.params?.comment_id
    );

    return reply
      .code(200)
      .send(SuccessResponse("Retrieved comment successfully", comment));
  } catch (error) {
    reply.send(error);
  }
};
