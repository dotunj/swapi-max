import Container, { Inject, Service } from "typedi";
import { CommentFactory } from "../factories/comment.factory";
import { CreateComment } from "../interfaces";
import { Comment } from "../entities/comment.entity";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { MovieService } from "./movie.service";
import { ResourceNotFoundException } from "../../../utils/errors";

@Service()
export class CommentService {
  constructor(
    @InjectRepository(Comment) private commentRepository: Repository<Comment>
  ) {}

  async findById(movieId: string, commentId: string): Promise<Comment> {
    const movieService = Container.get(MovieService)

    await movieService.findById(movieId);

    const comment = await this.commentRepository.findOne({ id: commentId });

    if (!comment) {
      throw new ResourceNotFoundException("Comment not found", 404);
    }

    return comment;
  }

  async findAll(movieId: string): Promise<Comment[]> {
    const movieService = Container.get(MovieService);

    await movieService.findById(movieId);

    return await this.commentRepository.find({
      where: { movie_id: movieId },
      order: { created_at: "DESC" },
    });
  }

  async create(attributes: CreateComment): Promise<Comment> {
    const comment = CommentFactory.create(attributes);

    return await this.commentRepository.save(comment);
  }

  async getMovieCount(movieId: string): Promise<number> {
    return await this.commentRepository
      .createQueryBuilder("comment")
      .where("comment.movie_id = :movieId", { movieId })
      .getCount();
  }
}
