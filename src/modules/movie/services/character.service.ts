import { Service } from "typedi";
import { MovieService } from "./movie.service";
import axios from "axios";
import { Character, Filter, QueryData } from "../interfaces";
import { CacheService } from "../../cache/services/cache.service";

@Service()
export class CharacterService {
  constructor(public movieService: MovieService, public cache: CacheService) {}

  async findAll(movieId: string, query: QueryData) {
    const movie = await this.movieService.findById(movieId);

    let characters: Character[] = [];

    for (const value of movie.characters) {
      const character = await this.fetchCharacter(value);

      characters.push({
        name: character.name,
        height: character.height,
        mass: character.mass,
        hair_color: character.hair_color,
        skin_color: character.skin_color,
        birth_year: character.birth_year,
        gender: character.gender,
      });
    }

    if (query.filter) {
      characters = this.filterCharacters(characters, query.filter);
    }

    if (query.sort) {
      characters = this.sortCharacters(characters, query.sort);
    }

    const metaData = this.fetchCharacterMetadata(characters);

    return { characters, metaData };
  }

  async fetchCharacter(url: string): Promise<Character> {
    let character = this.cache.get(url);

    if (character) {
      return character;
    }

    const { data } = await axios.get(url);

    this.cache.set(url, data);

    return data;
  }

  filterCharacters(characters: Character[], filter: Filter): Character[] {
    return characters.filter((character) => character.gender === filter);
  }

  sortCharacters(characters: Character[], sort: string): Character[] {
    const sortDirection = sort.startsWith("-") ? "DESC" : "ASC";

    sort = sort.replace("-", "");

    return characters.sort(function (a, b) {
      let upperBound = sort === "height" ? Number(b[sort]) : b[sort];
      let lowerBound = sort === "height" ? Number(a[sort]) : a[sort];

      if (sortDirection === "ASC") {
        if (lowerBound < upperBound) {
          return -1;
        }

        if (lowerBound > upperBound) {
          return 1;
        }

        return 0;
      }

      if (lowerBound < upperBound) {
        return 1;
      }

      if (lowerBound > upperBound) {
        return -1;
      }

      return 0;
    });
  }

  fetchCharacterMetadata(characters: Character[]) {
    const totalNumber = characters.length;
    const totalHeight = characters.reduce(function (a, b) {
      return a + Number(b.height);
    }, 0);

    return {
      total_number: totalNumber,
      total_height_in_cm: totalHeight,
    };
  }
}
