import Container, { Inject, Service } from "typedi";
import axios from "axios";
import { CacheService } from "../../cache/services/cache.service";
import { Movie } from "../interfaces";
import { ResourceNotFoundException } from "../../../utils/errors";
import { CommentService } from "./comment.service";

@Service()
export class MovieService {
  public baseUrl = `https://swapi.dev/api`;

  constructor(
    public cache: CacheService
) {
}

  async findAll(): Promise<Movie[]> {
    const commentService = Container.get(CommentService);
    
    const movies = await this.fetchMovies();

    const items: Movie[] = [];

    for (const movie of movies) {
      items.push({
        id: movie.episode_id,
        title: movie.title,
        opening_crawl: movie.opening_crawl,
        release_date: movie.release_date,
        characters: movie.characters,
        total_comments: await commentService.getMovieCount(
          movie.episode_id
        ),
      });
    }

    items.sort(function (a, b) {
      let breleaseDate: any = new Date(b.release_date);
      let areleaseDate: any = new Date(a.release_date);

      return areleaseDate - breleaseDate;
    });

    return items;
  }

  async findById(id: string): Promise<Movie> {
    const movies = await this.fetchMovies();

    const movie = movies.find((movie) => movie.episode_id == id);

    if (!movie) {
      throw new ResourceNotFoundException("Movie not found", 404);
    }

    return {
      id: movie.episode_id,
      title: movie.title,
      opening_crawl: movie.opening_crawl,
      release_date: movie.release_date,
      characters: movie.characters,
    };
  }

  async fetchMovies() {
    const key = "movies";

    let movies = this.cache.get(key);

    if (movies) {
      return movies;
    }

    const { data } = await axios.get(`${this.baseUrl}/films`);

    this.cache.set(key, data.results);

    return data.results;
  }
}
