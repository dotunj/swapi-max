import { CreateComment } from "../interfaces";
import { Comment } from "../entities/comment.entity";

export class CommentFactory {
  static create(attributes: CreateComment) {
    const comment = new Comment();

    comment.message = attributes.message;
    comment.ip_address = attributes.ip_address;
    comment.movie_id = attributes.movie_id;

    return comment;
  }
}
