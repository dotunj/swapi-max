export const ListCharacterResponseSchema = {
  description: "Fetch all Movie Characters",
  tags: ["character"],
  querystring: {
    type: "object",
    properties: {
      sort: { type: "string", enum: ["-height", "height", "-name", "name"] },
      filter: { type: "string", enum: ["male", "female"] },
    },
  },
  response: {
    "2xx": {
      type: "object",
      properties: {
        success: { type: "boolean" },
        message: { type: "string" },
        data: {
          type: "object",
          properties: {
            characters: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  name: { type: "string" },
                  height: { type: "string" },
                  mass: { type: "string" },
                  hair_color: { type: "string" },
                  skin_color: { type: "string" },
                  birth_year: { type: "string" },
                  gender: { type: "string" },
                },
              },
            },
            meta_data: {
              type: "object",
              properties: {
                total_number: { type: "number" },
                total_height_in_cm: { type: "number" },
              },
            },
          },
        },
      },
    },
  },
};
