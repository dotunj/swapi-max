export const ListMovieResponseSchema = {
  description: "List all movies",
  tags: ["movie"],
  response: {
    "2xx": {
      type: "object",
      properties: {
        success: { type: "boolean" },
        message: { type: "string" },
        data: {
          type: "object",
          properties: {
            movies: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  id: { type: "string" },
                  title: { type: "string" },
                  opening_crawl: { type: "string" },
                  release_date: { type: "string" },
                  total_comments: { type: "number" },
                },
              },
            },
          },
        },
      },
    },
  },
};

export const FindMovieResponseSchema = {
  description: "Find Movie",
  tags: ["movie"],
  response: {
    "2xx": {
      type: "object",
      properties: {
        success: { type: "boolean" },
        message: { type: "string" },
        data: {
          properties: {
            id: { type: "string" },
            title: { type: "string" },
            opening_crawl: { type: "string" },
            release_date: { type: "string" },
          },
        },
      },
    },
  },
};
