export const CreateCommentSchema = {
  description: "Create Comment for a Movie",
  tags: ["comment"],
  body: {
    type: "object",
    required: ["message"],
    properties: {
      message: { type: "string" },
    },
  },
  response: {
    "2xx": {
      type: "object",
      properties: {
        success: { type: "boolean" },
        message: { type: "string" },
        data: {
          properties: {
            id: { type: "string" },
            movie_id: { type: "string" },
            message: { type: "string" },
            ip_address: { type: "string" },
            created_at: { type: "string" },
          },
        },
      },
    },
  },
};

export const ListCommentsSchema = {
  description: "Fetch all comments for a movie",
  tag: ["comment"],
  response: {
    "2xx": {
      type: "object",
      properties: {
        success: { type: "boolean" },
        message: { type: "string" },
        data: {
          type: "object",
          properties: {
            comments: {
              items: {
                type: "object",
                properties: {
                  id: { type: "string" },
                  movie_id: { type: "string" },
                  message: { type: "string" },
                  ip_address: { type: "string" },
                  created_at: { type: "string" },
                },
              },
            },
          },
        },
      },
    },
  },
};

export const FetchCommentSchema = {
  description: "Fetch Comment",
  tags: ["comment"],
  response: {
    "2xx": {
      type: "object",
      properties: {
        success: { type: "boolean" },
        message: { type: "string" },
        data: {
          properties: {
            id: { type: "string" },
            movie_id: { type: "string" },
            message: { type: "string" },
            ip_address: { type: "string" },
            created_at: { type: "string" },
          },
        },
      },
    },
  },
};
