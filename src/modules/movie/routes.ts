import { SuccessResponse } from "../../utils";
import { ListCharacterResponseSchema } from "./schemas/character.schema";
import { CreateCommentSchema, ListCommentsSchema, FetchCommentSchema } from "./schemas/comment.schema";
import {
  ListMovieResponseSchema,
  FindMovieResponseSchema,
} from "./schemas/movie.schema";
const movieHandler = require("./handlers/movie.handler");
const characterHandler = require("./handlers/character.handler");
const commentHandler = require("./handlers/comment.handler");

async function routes(fastify, opts, done) {
  fastify.get("/", (req, reply) => {
    reply.send(
      SuccessResponse("Welcome to Swapi", {
        service: process.env.npm_package_name,
        version: process.env.npm_package_version,
      })
    );
  });

  fastify.get(
    "/movies",
    { schema: ListMovieResponseSchema },
    movieHandler.listAllMovies
  );
  fastify.get(
    "/movies/:movie_id",
    { schema: FindMovieResponseSchema },
    movieHandler.findMovie
  );
  fastify.get(
    "/movies/:movie_id/characters",
    { schema: ListCharacterResponseSchema },
    characterHandler.listAllCharacters
  );
  fastify.post(
    "/movies/:movie_id/comments",
    { schema: CreateCommentSchema },
    commentHandler.CreateComment
  );
  fastify.get(
    "/movies/:movie_id/comments",
    { schema: ListCommentsSchema },
    commentHandler.ListComments
  );
  fastify.get(
    "/movies/:movie_id/comments/:comment_id",
    { schema: FetchCommentSchema },
    commentHandler.FindComment
  );

  done();
}

module.exports = routes;
