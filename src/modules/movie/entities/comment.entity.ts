import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  Timestamp,
  UpdateDateColumn,
} from "typeorm";

@Entity("comments")
export class Comment {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: "varchar" })
  movie_id: string;

  @Column({ type: "varchar", length: 500 })
  message: string;

  @Column({ type: "varchar", nullable: true })
  ip_address?: string;

  @CreateDateColumn()
  created_at: Timestamp;

  @UpdateDateColumn()
  updated_at: Timestamp;
}
