export interface Movie {
    id: number;
    title: string;
    opening_crawl: string;
    release_date: string;
    comment_count?: number;
    characters: string[];
    total_comments?: number;
}

export interface Character {
    name: string;
    height: string;
    mass: string;
    hair_color: string;
    skin_color: string;
    birth_year: string;
    gender: string;
}

export interface QueryData {
    sort?: string;
    filter?: Filter;
}

export interface CreateComment {
    message: string;
    movie_id: string;
    ip_address: string;
}

export type Filter = 'male' | 'female'