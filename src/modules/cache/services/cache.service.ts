import { Service } from "typedi";
import * as NodeCache from "node-cache";

@Service()
export class CacheService {
  private client: any;

  constructor() {
    if (!this.client) {
      this.client = new NodeCache();
    }
  }

  set(key: string, data: object, ttl?: number): boolean {
    return this.client.set(key, data);
  }

  get(key: string): any | undefined {
    return this.client.get(key);
  }

  del(key: string) {
    return this.client.del(key);
  }
}
