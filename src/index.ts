import createServer  from "./server";
const server = createServer();

const start = async () => {
  try {
    await server.listen(process.env.PORT || 3000, "0.0.0.0");
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};

start();
