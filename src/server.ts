import "reflect-metadata";
import Fastify from "fastify";

function createServer() {
  const fastify = Fastify({
    logger: true,
  });

  fastify.register(require("./plugins/db"));
  fastify.register(require("./modules/movie/routes"), { prefix: "api/v1" });

  return fastify;
}

export default createServer;
