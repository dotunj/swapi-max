export function SuccessResponse(message: string, data?: object, meta?: object) {
  return {
    success: true,
    message,
    data,
    meta,
  };
}

export function ErrorResponse(
  message: string,
  statusCode?: number,
  errors?: []
) {
  return {
    success: false,
    message,
    statusCode,
    errors: errors?.length ? errors : [],
  };
}
