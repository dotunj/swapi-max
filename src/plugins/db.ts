import fastifyPlugin from 'fastify-plugin';
import { createConnection, useContainer } from 'typeorm';
import { getConnectionOptions } from 'typeorm';
import { Container } from 'typeorm-typedi-extensions';
import { Comment } from '../modules/movie/entities/comment.entity';

useContainer(Container);

async function createDBConnection() {
    try {
        const connection = await getConnectionOptions();
        Object.assign(connection, {
            entities: [Comment]
        })
        await createConnection(connection);
    } catch (error) {
        console.log(error)
    }
}

module.exports = fastifyPlugin(createDBConnection)