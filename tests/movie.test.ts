import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import createServer from "../src/server";
const server = createServer();
const baseUrl = `https://swapi.dev/api`;

describe("Movies", function () {
  let mock;

  const movie = {
    results: [
      {
        title: "A New Hope",
        episode_id: 4,
        opening_crawl: "a new opening",
        release_date: "1977-05-25",
        characters: ["https://swapi.dev/api/people/1/"],
      },
    ],
};

  beforeAll(async () => {
    await server.ready();
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
      mock.resetHandlers();
  })

  afterAll(async () => {
    await server.close();
  });

  test("it can fetch a list of movies", async () => {
    mock.onGet(`${baseUrl}/films`).replyOnce(200, movie)

    const response = await server.inject({
        method: "GET",
        url: "/api/v1/movies",
      });

    expect(response.statusCode).toEqual(200);
    expect(response.json().success).toBe(true)
    expect(response.json().data.movies[0].id).toEqual("4");
    expect(response.json().data.movies[0].title).toEqual('A New Hope');
  });

  test("it can fetch a single movie", async () => {
    mock.onGet(`${baseUrl}/films`).replyOnce(200, movie)

    const response = await server.inject({
        method: "GET",
        url: "/api/v1/movies/4",
      });

    expect(response.statusCode).toEqual(200);
    expect(response.json().success).toBe(true)
    expect(response.json().data.id).toEqual("4");
    expect(response.json().data.title).toEqual('A New Hope');
  })
});
