import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import createServer from "../src/server";
const server = createServer();
const baseUrl = `https://swapi.dev/api`;

describe("Characters", function () {
  let mock;

  const characters = [
    {
      name: "Luke",
      height: 12,
      mass: 50,
      hair_color: "black",
      skin_color: "black",
      birth_year: "1854",
      gender: "male",
    },
    {
        name: "SkyWalker",
        height: 30,
        mass: 40,
        hair_color: "black",
        skin_color: "black",
        birth_year: "1854",
        gender: "female",
      },
  ];

  const movie = {
    results: [
      {
        title: "A New Hope",
        episode_id: 4,
        opening_crawl: "a new opening",
        release_date: "1977-05-25",
        characters: ["https://swapi.dev/api/people/1", "https://swapi.dev/api/people/2"],
      },
    ],
  };

  beforeAll(async () => {
    await server.ready();
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.resetHandlers();
  });

  afterAll(async () => {
    await server.close();
  });

  test("it can fetch a list of movie characters", async () => {
    mock.onGet(`${baseUrl}/films`).replyOnce(200, movie);
    mock.onGet(`${baseUrl}/people/1`).replyOnce(200, characters[0]);
    mock.onGet(`${baseUrl}/people/2`).replyOnce(200, characters[1]);

    const response = await server.inject({
      method: "GET",
      url: "/api/v1/movies/4/characters",
    });

    expect(response.statusCode).toEqual(200);
    expect(response.json().success).toBe(true);
    expect(response.json().data.characters[0].name).toEqual('Luke');
    expect(response.json().data.characters[1].height).toEqual("30");
  });
});
